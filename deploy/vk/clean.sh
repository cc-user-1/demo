terraform init -input=false
terraform state rm vkcs_db_database.main_db
terraform state rm vkcs_db_user.main_db_user
terraform destroy --auto-approve -input=false \
   -var="vkcs_username=${VKCS_USERNAME}" \
   -var="vkcs_password=${VKCS_PASSWORD}" \
   -var="vkcs_project_id=${VKCS_PROJECT}" \
   -var="vkcs_region=${VKCS_REGION}" \
   -var="image_name="
