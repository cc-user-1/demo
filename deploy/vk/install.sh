terraform init -input=false
terraform apply --auto-approve -input=false \
  -var="vkcs_username=${VKCS_USERNAME}" \
  -var="vkcs_password=${VKCS_PASSWORD}" \
  -var="vkcs_project_id=${VKCS_PROJECT}" \
  -var="vkcs_region=${VKCS_REGION}" \
  -var="image_name=${CC_IMAGE_NAME}:${CC_VERSION_TAG}"
